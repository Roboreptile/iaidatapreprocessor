import pydicom as dicomio
import SimpleITK as sitk
from PIL import Image
import cv2
import numpy as np
import os
from xml.etree import ElementTree
from consts import *
from typing import Dict, Tuple


def roi2rect(img_array:np.ndarray, img_boxes:np.ndarray) -> Tuple[np.ndarray, np.ndarray]:
    matrix = np.zeros((N,N))

    scale_horizontal = img_array.shape[0]/DEST_SIZE[1]
    scale_vertical = img_array.shape[1]/DEST_SIZE[0]

    resized = cv2.resize(src=img_array, dsize=DEST_SIZE)
    resized = np.array(resized)/65536

    for i in range(img_boxes.shape[0]):

        bounding_box = img_boxes[i,:]

        xmin = bounding_box[0]/scale_horizontal
        ymin = bounding_box[1]/scale_vertical
        xmax = bounding_box[2]/scale_horizontal
        ymax = bounding_box[3]/scale_vertical
        width = bounding_box[4]/scale_horizontal
        height = bounding_box[5]/scale_vertical

        iterX = width/N
        iterY = height/N
        
        for i in range(N):
            for j in range(N):
                if iterX*i>xmin and iterX*i<xmax and iterY*j<ymax and iterY*j>ymin:
                    matrix[i][j]=1

    return matrix, resized


def getUID_path(path:str) -> Dict[str, Tuple[str,str]]:

    def loadFileInformation(filename:str) -> str:
        ds = dicomio.read_file(filename, force=False)
        return ds.SOPInstanceUID #this is name of the file without extension, I think

    dict = {}
    list = os.listdir(path)

    for date in list:
        date_path = os.path.join(path, date)
        series_list = os.listdir(date_path)

        for series in series_list:
            series_path = os.path.join(date_path, series)
            dicom_list = os.listdir(series_path)

            for dicom in dicom_list:
                dicom_path = os.path.join(series_path, dicom)
                dicom_num_info = loadFileInformation(dicom_path)
                dict[dicom_num_info] = dicom_path

    return dict




def matrixToImage(data: np.ndarray) -> np.ndarray:
    data = (data + 1024) * 0.125
    img_rgb = data.astype(np.uint8)
    return img_rgb


def loadFile(filename:str) -> Tuple[np.ndarray, int, int]:
    ds = sitk.ReadImage(filename)
    img_array = sitk.GetArrayFromImage(ds)

    _, width, height = img_array.shape
    return img_array[0], width, height


def processXMLDirectory(data_path:str) -> Dict[str,np.ndarray]:

    data = dict()
    filenames = os.listdir(data_path)

    for filename in filenames:

        tree = ElementTree.parse(os.path.join(data_path, filename))
        root = tree.getroot()

        size_tree = root.find('size')
        objects = root.findall('object')

        width = int(size_tree.find('width').text)
        height = int(size_tree.find('height').text)

        bounding_boxes = np.empty((len(objects),6))
        for idx,object_tree in enumerate(objects):
            for bounding_box in object_tree.iter('bndbox'):

                    xmin = int(bounding_box.find('xmin').text)
                    ymin = int(bounding_box.find('ymin').text)
                    xmax = int(bounding_box.find('xmax').text)
                    ymax = int(bounding_box.find('ymax').text)

            bounding_boxes[idx,:] = np.array([xmin, ymin, xmax, ymax, width, height]) 

        data[filename[:-4]] = bounding_boxes

    return data
