from pandas import array
from tqdm import tqdm
from get_data_from_XML import *
import pickle


dicom_path          = f"{DATA_FOLDER}/Lung-PET-CT-Dx"
annotation_path     = f"{DATA_FOLDER}/Annotation"

if __name__ == '__main__':
    errs = 0
    corrs = 0

    if not os.path.exists(RESULT_FOLDER):
        os.mkdir(RESULT_FOLDER)

    dirs = os.listdir(annotation_path)

    with open(OUTPUT_FILE, "w") as output_file:

        for annotation_id in tqdm(
                dirs,
                desc="[INFO] Processing data",
                total=len(dirs)
            ):

            if(MAX_COUNT!=-1 and corrs >= MAX_COUNT):
                break

            uid_path = f"{dicom_path}/Lung_Dx-{annotation_id}"
            bbox_path = f"{annotation_path}/{annotation_id}"

            if not os.path.isdir(uid_path) or not os.path.isdir(bbox_path): continue
            
            try:
                path_dict  = getUID_path(uid_path)           # uid(name) -> (path, name)
                bbox_dict  = processXMLDirectory(bbox_path)  # name -> ndimarray bbox
            except:
                continue
            
            items = bbox_dict.items()
            for name, bboxes in tqdm(items,desc=f"[INFO] Data folder: {annotation_id}",total=len(items), leave=False):

                try:
                    dcm_path = path_dict[name]
                except:
                    errs += 1
                    continue

                try:
                    matrix, width, height = loadFile(dcm_path)
                    img_bitmap = matrixToImage(matrix)
                    matrix, resized_img = roi2rect(img_bitmap, bboxes)

                    data_name  = f"{RESULT_FOLDER}/{corrs}_data.npy"
                    label_name = f"{RESULT_FOLDER}/{corrs}_label.npy"
                    np.save(data_name,matrix)
                    np.save(label_name,matrix)

                    output_file.write(f"{data_name} {label_name}\n")

                    corrs += 1
                except:
                    errs += 1
                    continue

        print(f"Corrs: {corrs}, Errs: {errs}, Sum: {corrs+errs}")